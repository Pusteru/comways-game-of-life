import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertTrue;

public class ConwayLifeTest {

    @Test
    public void testGlider() {

        int[][][] gliders = {
                {{1, 0, 0},
                        {0, 1, 1},
                        {1, 1, 0}},

                {{0, 1, 0},
                        {0, 0, 1},
                        {1, 1, 1}}
        };
        int[][] mat = new int[20][20];
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                mat[i][j] = Math.random() < 0.5 ? 0 : 1;
            }
        }
        System.out.println("Glider");
        //LifeDebug.print(gliders[0]);
        int[][] res = ConwayLife.getGeneration(mat, 5000);
        assertTrue("Got \n" + LifeDebug.htmlize(res) + "\ninstead of\n" + LifeDebug.htmlize(gliders[1]), LifeDebug.equals(res, gliders[1]));
    }

}