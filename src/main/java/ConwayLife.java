import java.util.Arrays;

class ConwayLife {


    static int[][] getGeneration(int[][] glider, int i) {
        int[][] post = Arrays.stream(glider).map(int[]::clone).toArray(int[][]::new);
        for (int iteration = 0; iteration < i; iteration++) {
            if (Arrays.stream(glider).mapToInt(arr -> arr[0]).sum() == 0) return new int[0][0];
            glider = computeIteration(glider, post);
        }
        return post;
    }

    private static int[][] computeIteration(int[][] glider, int[][] post) {
        for (int row = 0; row < glider.length; row++) {
            for (int col = 0; col < glider[0].length; col++) {
                int neighboursAlive = getNumberOfAliveNeighbours(glider, row, col);
                int survival = getSurvival(glider[row][col], neighboursAlive);
                post[row][col] = survival;
            }
        }
        return post;
    }

    private static int getSurvival(int pos, int neighboursAlive) {
        return neighboursAlive == 3 || (pos == 1 && neighboursAlive == 2) ? 1 : 0;
    }

    private static int getNumberOfAliveNeighbours(int[][] glider, int row, int col) {
        int sum = -glider[row][col];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int r = (glider.length + (row - 1 + i)) % glider.length;
                int c = (glider[0].length + (col - 1 + j)) % glider[0].length;
                sum += glider[r][c];
            }
        }
        return sum;
    }
}
