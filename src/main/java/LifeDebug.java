import java.util.Arrays;

class LifeDebug {
    static void print(int[][] glider) {
        String value = Arrays.deepToString(glider).replace("], ", "]\n").replace("[[", "[").replace("]]", "]");
        System.out.println(value.replaceAll("[]|\\[,]", "").replaceAll("0", ".").replaceAll("1", "*"));
    }

    static String htmlize(int[][] res) {
        return Arrays.deepToString(res).replace("], ", "]\n").replace("[[", "[").replace("]]", "]");
    }

    static boolean equals(int[][] res, int[][] glider) {
        return Arrays.equals(res, glider);
    }
}
